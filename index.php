<?php 

	// Declaring interface class
	interface Session
	{
	    public function execute();
	}

	// Create a factory class for session
	class SaveToDatabase implements Session
	{
	    public function execute(){
	        echo '<b>This Data is saved to Database</b>';
	    }
	}

	// Create a factory class for session
	class SaveToFile implements Session
	{
	    public function execute(){
	        echo '<b>This Data is saved to File</b>';
	    }
	}

	// Create a factory class for session
	class SaveToJson implements Session
	{
	    public function execute(){
	        echo '<b>This Data is saved to JSON</b>';
	    }
	}

	// Declare an abstract class
	abstract class Asselmbe
	{
	    protected $session;
	    
	    public function __construct(Session $session)
	    {
	        $this->session = $session;
	    }


		public function output()
		{
			$this->addWheel()->addBody()->addParts()->extraFeature();
			return $this->session->execute();
		}

		abstract protected function extraFeature();

		public function addWheel()
		{
			echo "<br>";
			echo "-------------------------------------------";
			echo "<br>";
			echo "Add wheel in the vehicle";
			echo "<br>";
			return $this;
		}

		public function addBody()
		{
			echo "Add body in the vehicle";
			echo "<br>";
			return $this;
		}

		public function addParts()
		{
			echo "Add parts in the vehicle";
			echo "<br>";
			return $this;
		}
	}


	//  Car class extending the abstract assemble class
	class Car extends Asselmbe
	{

		public function extraFeature()
		{
			echo "Add a frontglass";
			echo "<br>";
		}
	}

	//  Bike class extending the abstract assemble class
	class Bike extends Asselmbe
	{

		public function extraFeature()
		{
			echo "Add a Headlight";
			echo "<br>";
		}
	}

	$controller = new Car(new SaveToJson); // passing the Factory class here
	$controller->output();

	$controller = new bike(new SaveToFile); // passing the Factory class here
	$controller->output();

	



				//  OUTPUT  //
/**	
		-------------------------------------------
		Add wheel in the vehicle
		Add body in the vehicle
		Add parts in the vehicle
		Add a frontglass
		This Data is saved to JSON
		-------------------------------------------
		Add wheel in the vehicle
		Add body in the vehicle
		Add parts in the vehicle
		Add a Headlight
		This Data is saved to File
**/


?>